import "reflect-metadata";
import * as moment from "moment";
import { GraphQLServer } from "graphql-yoga";
import { buildSchema } from "type-graphql";
import AuthorResolver from "./app/resolvers/AuthorResolver";
import BookResolver from "./app/resolvers/BookResolver";
import { db } from "./app/database/db";
import { CONFIG_SERVER } from "./config";

db.then(async (e) => {
  const schema = await buildSchema({
    resolvers: [BookResolver, AuthorResolver],
    emitSchemaFile: true,
  });

  const server = new GraphQLServer({
    schema,
    context: e,
  });

  server.start({ port: CONFIG_SERVER.port }, () => {
    console.log(
      `[${moment().format(
        "DD.MM.YYYY hh:mm:ss"
      )}] Server is running on http://localhost:${CONFIG_SERVER.port}`
    );
  });
});
