import { Field, Int, ObjectType, ID, ArgsType } from "type-graphql";

@ObjectType()
export class Author {
  @Field((type) => ID)
  authorId: number;

  @Field()
  name: string;
}