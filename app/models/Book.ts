import { IsPositive, Max, Min } from "class-validator";
import { ArgsType, InputType, Field, Int, ObjectType, ID } from "type-graphql";
import { Author } from "./Author";

@ObjectType()
export class Book {
  @Field((type) => ID)
  bookId: number;

  @Field()
  name: string;

  @Field((type) => ID, { nullable: true })
  authorId: number;

  @Field((type) => Int)
  pageCount: number;

  @Field((type) => Author, { nullable: true })
  author: Author;
}
