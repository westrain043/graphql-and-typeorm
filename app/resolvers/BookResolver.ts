import {
  Arg,
  FieldResolver,
  Mutation,
  Query,
  Resolver,
  Root,
  Ctx,
} from "type-graphql";
import { Book } from "../Models/Book";
import { BookEntity } from "../database/entities/book.entity";
import { AuthorEntity } from "../database/entities/author.entity";
import { Connection } from "typeorm";
import AuthorResolver from "./AuthorResolver";
import { Author } from "../Models/Author";

@Resolver(Book)
export default class {
  @Query((returns) => [Book], { nullable: true })
  async books(
    @Arg("skip", { nullable: true }) skip: number = 0,
    @Arg("take", { nullable: true }) take: number = 5,
    @Ctx() ctx: Connection
  ) {
    //c join'ом, без FieldResolver
    /* return await ctx.manager
      .createQueryBuilder(BookEntity, "books")
      .leftJoinAndSelect("books.author", "author")
      .getMany(); */
    return await ctx.manager
      .createQueryBuilder(BookEntity, "books")
      .skip(skip)
      .take(take)
      .getMany();
  }

  @FieldResolver((returns) => Author, { nullable: true })
  async author(@Root() book: BookEntity, @Ctx() ctx: Connection) {
    return (
      await ctx.manager.createQueryBuilder(AuthorEntity, "author").getMany()
    )
      .filter((e) => {
        return e.authorId === book.authorId;
      })
      .pop();
  }

  @Mutation((of) => Book)
  async createBook(
    @Arg("name") name: string,
    @Ctx() ctx: Connection,
    @Arg("author", { nullable: true }) authorName: string | null = null
  ) {
    const book = new BookEntity();
    book.name = name;

    if (authorName) {
      const authorFound = await AuthorResolver.prototype.findAuthor(ctx, {
        where: { name: authorName },
      });

      if (authorFound) book.authorId = authorFound.authorId;
      else {
        book.authorId = (
          await AuthorResolver.prototype.createAuthor(authorName, ctx)
        ).authorId;
      }
    }

    return await ctx.manager.save(book);
  }
}
