import {
  Arg,
  Mutation,
  Resolver,
  Ctx,
} from "type-graphql";
import { Author } from "../Models/Author";
import { Connection} from "typeorm";
import { AuthorEntity } from "../database/entities/author.entity";

@Resolver(Author)
export default class {
  @Mutation((of) => Author)
  async createAuthor(
    @Arg("name") name: string,
    @Ctx() ctx: Connection
  ) {
    const author = new AuthorEntity();
    author.name = name;
    return await ctx.manager.save(author);
  }

  async findAuthor(@Ctx() ctx: Connection, where) {
    return await ctx.manager.getRepository(AuthorEntity).findOne(where);
  }
}
