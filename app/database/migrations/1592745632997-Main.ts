import {MigrationInterface, QueryRunner} from "typeorm";

export class Main1592745632997 implements MigrationInterface {
    name = 'Main1592745632997'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("CREATE TABLE `author` (`authorId` int NOT NULL AUTO_INCREMENT, `name` varchar(150) NOT NULL, UNIQUE INDEX `IDX_d3962fd11a54d87f927e84d108` (`name`), PRIMARY KEY (`authorId`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `books` (`bookId` int NOT NULL AUTO_INCREMENT, `name` varchar(150) NOT NULL, `authorId` int NULL, PRIMARY KEY (`bookId`)) ENGINE=InnoDB");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("DROP TABLE `books`");
        await queryRunner.query("DROP INDEX `IDX_d3962fd11a54d87f927e84d108` ON `author`");
        await queryRunner.query("DROP TABLE `author`");
    }

}
