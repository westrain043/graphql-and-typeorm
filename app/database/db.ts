import * as moment from "moment";
import { createConnection, ConnectionOptions } from "typeorm";

import { BookEntity } from "./entities/book.entity";
import { AuthorEntity } from "./entities/author.entity";
import ormconfig = require("../../ormconfig");

let OPTIONS = <ConnectionOptions>ormconfig;

export const db = createConnection({
  ...OPTIONS,
  entities: [BookEntity, AuthorEntity],
})
  .then(async (e) => {
    console.log(
      `[${moment().format(
        "DD.MM.YYYY hh:mm:ss"
      )}] Connection established with ${OPTIONS.type} database`
    );

    return e;
  })
  .catch((e) => {
    console.log(
      `[${moment().format("DD.MM.YYYY hh:mm:ss")}] Database error: ${e.message}`
    );

    process.exit(1);
  });
