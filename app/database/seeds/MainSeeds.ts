import {Connection} from "typeorm";
import { db } from "../db";

export const MainSeeds = async (conn:Connection)=>{

    await conn.query("INSERT INTO `author` (`name`) VALUES ('Лев Толстой');").then(async e=>{
        await conn.query("INSERT INTO `books` (`name`, `authorId`) VALUES ('Война и мир', "+e.insertId+");");
        return conn.query("INSERT INTO `books` (`name`, `authorId`) VALUES ('Анна Каренина', "+e.insertId+");");
    });
    await conn.query("INSERT INTO `author` (`name`) VALUES ('Джордж Оруэлл');").then(async e=>{
        await conn.query("INSERT INTO `books` (`name`, `authorId`) VALUES ('1984', "+e.insertId+");");
       
    });
    await conn.query("INSERT INTO `author` (`name`) VALUES ('Оскара Уайльда');").then(async e=>{
        return conn.query("INSERT INTO `books` (`name`, `authorId`) VALUES ('Портре́т Дориа́на Гре́я', "+e.insertId+");");
    });
    await conn.query("INSERT INTO `author` (`name`) VALUES ('Харпер Ли');").then(async e=>{
        return conn.query("INSERT INTO `books` (`name`, `authorId`) VALUES ('Убить пересмешника', "+e.insertId+");");
    });
    await conn.query("INSERT INTO `author` (`name`) VALUES ('Карлос Руис Сафон');").then(async e=>{
        await conn.query("INSERT INTO `books` (`name`, `authorId`) VALUES ('Тень ветра', "+e.insertId+");");
        return conn.query("INSERT INTO `books` (`name`, `authorId`) VALUES ('Игра ангела', "+e.insertId+");");
    });        

}

db.then(async e=>{
    await MainSeeds(e);
    await e.close();
});
