import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from "typeorm";
/* import { BookEntity } from "./book.entity"; */

@Entity({ name: "author" })
export class AuthorEntity {
  @PrimaryGeneratedColumn()
  authorId: number;

  @Column({ type: "varchar", unique: true, length: 150 })
  name: string;

  /* @OneToMany(() => BookEntity, (book: BookEntity) => book.author)
  books: BookEntity[]; */
}
