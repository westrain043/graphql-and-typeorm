import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  JoinColumn,
} from "typeorm";

/* import { AuthorEntity } from "./author.entity"; */

@Entity({ name: "books" })
export class BookEntity {
  @PrimaryGeneratedColumn()
  bookId: number;

  @Column({ type: "varchar", length: 150 })
  name: string;

  /*  @ManyToOne(() => AuthorEntity, (author: AuthorEntity) => author.books)
  @JoinColumn({ name: "authorId" })
  author: AuthorEntity; */

  @Column("int", { nullable: true })
  authorId: number;
}
