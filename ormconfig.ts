export = {
  host: process.env.DB_HOST || "localhost",
  type: "mysql",
  port: parseInt(process.env.PORT, 10) || 3306,
  username: process.env.DB_USERNAME || "admin",
  password: process.env.DB_PASSWORD || "111",
  database: process.env.DB_NAME || "BigLibrary",
  entities: ["./app/database/entities/*.entity.ts"],
  migrations: ["./app/database/migrations/*.ts"],
  cli: {
    migrationsDir: "./app/database/migrations",
  },
  synchronize: false,
};
