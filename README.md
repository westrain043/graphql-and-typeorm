# graphql-and-typeorm

**Требуется**: nodejs, typescript, ts-node, mysql
<br/>
Версии которые использовались во время разработки:
<br/>
nodejs v12.16.1
<br/>
mysql v8.0.20
<br/>
typescript v3.7.5
<br/>
ts-node v8.6.2
<br/>

Базу данных создать заранее, по умолчанию "BigLibrary"
<br/>

**Установка**: npm i
<br/>
**Поднять миграции**: npm run migrate:up
<br/>
**Поcсев данных**: npm run seeds
<br/>
**Запуск сервера**: export DB_HOST=localhost DB_USERNAME=admin DB_PASSWORD=111 DB_NAME=BigLibrary && npm start
<br/>

Примеры запросов:

1 Получить список книг

```javascript
{
  books(take:10){
    name,
    author {
      name
    }
  }
}
```

2 Создать автора

```javascript
mutation {
  createAuthor(name:"Джон Р. Р. Толкин") {
    name
  }
}
```
